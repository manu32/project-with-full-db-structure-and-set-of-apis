var express = require('express');
var router = express.Router();

var func=require('./commonFunction');
var sendResponse=require('./sendResponse');
var async=require('async');
var md5 = require('MD5');
var moment=require('moment');

//signup the driver
router.post('/signup_driver',function(req,res,next){
    console.log(req.body);
    var firstName = req.body.first_name;
    var email = req.body.email;
    var password = req.body.password;
    var phonoNo=req.body.phonono;
    var address=req.body.address;
    var curLatitude=req.body.current_latitude;
    var curLongitude=req.body.current_longitude;
    var deviceType = req.body.device_type; // 1 for android, 2 for ios
    var deviceToken = req.body.device_token;
    var appVersion = req.body.app_version;  //1 for android.3 for ios
    console.log(req.body.file1);
    var userImage=req.files.file;
    var manValues=[firstName,email,password,phonoNo,address,curLatitude,curLongitude,deviceType,deviceToken,appVersion,userImage];
    var randomFileName=func.generateRandomNumber();
    req.files.file.name = randomFileName;
    // var userImage=req.files.file1;
    async.waterfall([
        function(callback) {
            func.checkBlank(res, manValues, callback);
        },
        function(callback) {
            func.checkEmailAvailability(res, email, callback);
        }
    ],function(err,result){
        console.log("async result",result);

        if(err){
            sendResponse.somethingWentWrongError(res);
        }
        else {
            var curLoginTime = new Date();
            var accessToken = func.encrypt(email + curLoginTime);
            var encryptPassword = md5(password);
            console.log("insert statement");

            func.uploadImage(res,userImage,function(path) {

                console.log(path);
                var sql = "INSERT INTO `tb_user`(`name`,`email`,`password`,`access_token`,`phonono`,`address`,`current_latitude`,`current_longitude`,`device_type`,`device_token`,`app_version`,`signup_time`,`current_status`,`image`,reg_as) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                var values = [firstName, email, encryptPassword, accessToken, phonoNo, address, curLatitude, curLongitude, deviceType, deviceToken, appVersion, curLoginTime, 0, path,1]; //1 for registered through driver

                dbConnection.Query(res, sql, values, function (result) {
                    console.log("execute the insert query");
                    driverId=result[0].id;
                    var data = {
                        access_token: accessToken,
                        first_name: firstName,
                        address_driver:address,
                        driver_id:driverId
                    };
                    func.sendMail(email, firstName, res, function (callback) {
                        if (callback == 1)
                            sendResponse.sendSuccessData(data, res);
                    });
                });
            });
        }

    });

});

// verify driver through the admin 0 for rejected and 1 for accepted
router.post('/verify_driver',function(req,res){
    var accessToken=req.body.access_token;
    var driverId=req.body.driver_id;
    var status=req.body.status_driver;
    var manValues=[accessToken,driverId,status];
    async.waterfall([
        function(callback) {
            func.checkBlank(res, manValues, callback);
        },
        function(callback) {
            func.authenticateAdmin(res,accessToken,callback)
        }
    ],function(resultAuthenticate) {
        if(resultAuthenticate==1){
              if(status==0){
                      sendResponse.sendErrorMessage(constant.responseMessage.REJECTED_DRIVER,res);
              }
              else if(status==1){
                      sql1 = 'update tb_user set `verified_status`=? where `id`=?';
                      var values = [1, driverId];
                      dbConnection.Query(res, sql1, values, function (result) {
                          var effectrrows = result.affectedRows;
                          console.log(effectrrows);
                          if (effectrrows == 0) {
                              sendResponse.sendErrorMessage(constant.responseMessage.NOT_ACCEPTED_DRIVER, res);
                          }
                          else {
                              sendResponse.successStatusMsg(constant.responseMessage.VERIFIED_DRIVER, res);
                          }
                      });
              }
            else{
                  sendResponse.sendErrorMessage(constant.responseMessage.ASSIGN_DRIVER,res);
              }
        }
        else{
            sendResponse.somethingWentWrongError(res);
        }

    });
});

router.post('/assign_driver',function(req,res){
    var accessToken=req.body.access_token;
    var pickupLatitude=req.body.latitude;
    var pickupLongitude=req.body.longitude;
    var userAddress=req.body.address;
    var driverAvailable=[];
    var manValues=[accessToken,pickupLatitude,pickupLongitude,userAddress];
    async.waterfall([
        function(callback){
            func.checkBlank(res,manValues,callback);
        },
        function(callback){
            func.authenticateUser(res,accessToken,callback);
        }
    ], function(callbackresult) {
       var userId=callbackresult[0].id;
        console.log("insert statement wrong",userId);
        var sql = "insert into `tb_order` (`user_id`,`user_address`,`start_latitude`,`start_longitude`,driver_status) VALUES(?,?,?,?)";
        var values=[userId,userAddress,pickupLatitude,pickupLongitude,1];
        dbConnection.Query(res,sql,values,function(result) {
            console.log("result"+result);
            if(result.length==0) {
                sendResponse.somethingWentWrongError(res);
            }
            else {
                console.log("something in wrong in select statement");
                sql1 = 'select `id`,`device_type`,`device_token`,`email`,`current_latitude`,`current_longitude`,`access_token` from  tb_user where `particular_area_driver_status`=?';//particular Area driver status 0 for not in current area and 1 for present in current area
                dbConnection.Query(res, sql1, [1], function (resultAvailableDriver) {
                    if (resultAvailableDriver.length == 0) {
                        sendResponse.somethingWentWrongError(res);
                    }
                    else {
                        console.log(resultAvailableDriver.length);

                        for (var i = 0; i < resultAvailableDriver.length; i++) {
                            var driverId = resultAvailableDriver[i].id;
                            var deviceType = resultAvailableDriver[i].device_type;
                            var deviceToken = resultAvailableDriver[i].device_token;
                            var driverEmail = resultAvailableDriver[i].email;
                            var accessToken = resultAvailableDriver[i].access_token;
                            driverAvailable[i] = {
                                'driverId': driverId,
                                'deviceType': deviceType,
                                'deviceToken': deviceToken,
                                'driverEmail': driverEmail,
                                'access_token': accessToken,
                                'user_id':userId
                            };
                        }
                        console.log(driverAvailable);
                        console.log(driverAvailable.length);
                        for (var i = 0; i < driverAvailable.length; i++) {
                            sendNotificationForEachDriver(userId, userAddress, deviceType, deviceToken, accessToken,pickupLatitude,pickupLongitude);
                        }

                        sql1='update tb_user set driver_request=? where id=?';        // 1 for request through the user to driver
                        dbConnection.Query(res,sql1,[1,userId],function(result){
                            var effectrows=result.affectedRows;
                            if(effectrows==0){
                                sendResponse.somethingWentWrongError(res);
                            }
                            else{
                                var data= {
                                    'user_id': userId,
                                    'Request':'waiting for confirmation to driving'
                                }
                                sendResponse.sendSuccessData(data,res);
                            }
                        });

                    }
                });
            }
        });
    });
});

function sendNotificationForEachDriver(userId,userAddress,deviceType,deviceToken,accessToken,pickuplatitude,pickuplongitude){
    console.log("send notification each driver");
}

router.post('/accepted_notification',function(req,res){
      var accessToken=req.body.access_token;
      var userId=req.body.user_id;
      var driverLatitude=req.body.latitude;
      var driverLongitude=req.body.longitude;
    var manValues=[accessToken];
    async.waterfall([
        function(callback){
             func.checkBlank(res,manValues,callback);
        },
        function(callback){
            func.authenticateDriver(res,accessToken,callback);
        }
    ],
       function(callbackresult){
          var driverId=callbackresult[0].id;
           var driverPhonono=callbackresult[0].phonno;
           var driverLatitude=callbackresult[0].current_latitude;
           var driverLongitude=callbackresult[0].current_longitude;
           sql1='update tb_user set idle_driver=?,driver_deals_with_customer=?,driver_request=?,last_order_id=? where id=? limit 1';
           var values=[1,1,2,userId,driverId];
           dbConnection.Query(res,sql1,values,function(result){
               var effectrows=result.affectedRows;
               if(effectrows==0){
                   sendResponse.somethingWentWrongError(res);
               }
               else{

                   var cur_date=moment().format();
                   var date=moment(cur_date).format('YYYY:MM:DD');
                   var time=moment(cur_date).format('HH:MM');
                   console.log(cur_date,date,time);
                   var waitingPickUpTimeToCustomer="";
                               var sql='update tb_user set driver_request=? where id=?';
                               dbConnection.Query(res,sql,[2,driverId],function(result){
                                   console.log(result.affectedRows);
                                  if(result.affectedRows==0){
                                      sendResponse.somethingWentWrongError(res);
                                  }
                                   else{
                                   sql1='select start_latitude,start_longitude from tb_order where user_id=?';
                                      dbConnection.Query(res,sql1,[userId],function(result){
                                          console.log(result.length);
                                           if(result.length==0){
                                               sendResponse.somethingWentWrongError(res);
                                           }
                                          else{
                                                user_Latitude=result[0].start_latitude;
                                                user_longitude=result[0].start_longitude;
                                            func.checkdistance(driverLatitude,driverLongitude,user_Latitude,user_longitude,function(waitingdist){
                                                 var min=2;   // 1 millis take the 2 min
                                                 waitingPickUpTimeToCustomer=waitingdist*2;
                                                var pickupTime = moment(cur_date).add('minutes', waitingPickUpTimeToCustomer).format();
                                                sql1='update tb_order set driver_starting_time=?,date=?,driver_picked_time=?,driver_status=?,driver_id=? where user_id=? && driver_status=?';
                                                dbConnection.Query(res,sql1,[cur_date,date,pickupTime,2,driverId,userId,1],function(result){
                                                    console.log("that is result "+result.affectedRows);
                                                    if(result.affectedRows==0){
                                                        sendResponse.somethingWentWrongError(res);
                                                    }
                                                    else{
                                                        data={
                                                         'driver_id':driverId,
                                                         'driver_phoneNo':driverPhonono,
                                                         'driver_latitude':driverLatitude,
                                                         'driver_longitude':driverLongitude,
                                                         'confirm_order':'confirm your order'
                                                         };
                                                         sendResponse.sendData(res,data);
                                                    }
                                                });
                                            });
                                           }
                                      });

                                  }

                               });
                           }
                       });
       }
    );
});

router.post('/start_service',function(req,res){
       var accessToken=req.body.access_token;
       //var driverLatitude=req.body.driver_latitude;
       //var driverLongitude=req.body.driver_longitude;
        var manValues=[accessToken];
       async.waterfall([
           function(callback){
             func.checkBlank(res,manValues,callback);
           },
           function(callback){
               func.authenticateDriver(res,accessToken,callback);
           }
       ],function(result){
               var cur_time=moment().format('YYYY-MM-DD HH:mm');
               console.log(cur_time);
               var sql1='select id,last_order_id,driver_request from tb_user where access_token=? limit 1';
               dbConnection.Query(res,sql1,[accessToken],function(callbackresult){
                   if(callbackresult==0){
                       sendResponse.somethingWentWrongError(res);
                   }
                   else{
                       var driverId=callbackresult[0].id;
                       var lastOrderId=callbackresult[0].last_order_id;
                       var driverRequest=callbackresult[0].driver_request;
                           console.log(driverId,lastOrderId,driverRequest);
                       var sql1='select driver_starting_time,driver_picked_time,order_id from tb_order where user_id=? && driver_id=? && driver_status=? limit 1';
                       dbConnection.Query(res,sql1,[lastOrderId,driverId,driverRequest],function(result){
                           console.log(result);
                            if(result.length==0){
                                sendResponse.somethingWentWrongError(res);
                            }
                           else{
                                var deliveryTime=result[0].driver_picked_time;
                                var orderId=result[0].order_id;
                                var driverStartingTime=result[0].driver_starting_time;
                                var startDate = moment(deliveryTime, 'YYYY-MM-DD HH:mm');
                                var endDate = moment(cur_time, 'YYYY-MM-DD HH:mm');
                                var Diff = endDate.diff(startDate, 'minutes');
                                console.log(Diff);
                                if(Diff>=0){
                                   var sql1='update tb_order set driver_status=? where order_id=?';
                                    dbConnection.Query(res,sql1,[3,orderId],function(rsult){
                                        if(rsult.affectedRows==0){
                                            sendResponse.somethingWentWrongError(res);
                                        }
                                       else{
                                            var sql='update tb_user set driver_request=? where id=?';
                                            dbConnection.Query(res,sql,[3,driverId],function(result){
                                                  if(result.affectedRows==0){
                                                      sendResponse.somethingWentWrongError(res);
                                                  }
                                                else{
                                                      data={
                                                           'driver_id':driverId,
                                                           'msg':'driver start the riding with user'
                                                      };
                                                      sendResponse.sendSuccessData(data,res);
                                                  }
                                            });
                                        }
                                    });
                                }
                              else{
                                    sendResponse.sendErrorMessage(constant.responseMessage.DRIVER_NOT_REACH,res);
                                }


                            }
                       });
                   }
               });
           }
       );


});
router.post('/end_service',function(req,res){
      var accessToken=req.body.access_token;
      var dropLatitude=req.body.drop_latitude;
      var dropLongitude=req.body.drop_longitude;
    var manValues=[accessToken,dropLatitude,dropLongitude];
     async.waterfall([
          function(callback){
              func.checkBlank(res,manValues,callback);
          },
         function(callback){
             func.authenticateDriver(res,accessToken,callback);
         }
       ], function(result){
            var driverId=result[0].id;
            var driverLastOrderID=result[0].last_order_id;
            var driverStatus=result[0].request_driver;
         if(driverLastOrderID==0){
             sendResponse.sendErrorMessage(constant.responseMessage.NO_ORDER,res);
         }
         else{
               var  finishTime=moment().format('YYYY-MM-DD HH:mm');
               var sql='select order_id from tb_order where user_id=? && driver_id=? && driver_status=?';
              dbConnection.Query(res,sql,values[driverLastOrderID,driverId,driverStatus],function(result){
                   if(result.length==0){
                       sendResponse.somethingWentWrongError(res);
                   }
                  else {
                       var orderId=result[0].id;
                       var sql = 'update tb_order set drop_latitude=?,drop_longitude=?,finish_timing=? where order_id=?';
                       var values = [dropLatitude, dropLongitude, finishTime, orderId];
                       dbCOnnection.Query(res, sql, values, function (result) {
                           if (result.affectedRows == 0) {
                               sendResponse.somethingWentWrongError(res);
                           }
                           else {
                               var sql1 = 'select driver_picked_time,finish_time from tb_order where order_id=?';
                               dbConnection.Query(res,sql1,[orderId],function(result){
                                   if(result.length==0){
                                       sendResponse.somethingWentWrongError(res);
                                   }
                                   else{
                                       var driverPickupTime=result[0].driver_picked_time;
                                        var driverFinishTime=result[0].finish_time;
                                       var a = moment((driverPickupTime), 'YYYY-MM-DD HH:mm');
                                       var a = moment((driverFinishTime), 'YYYY-MM-DD HH:mm');
                                        var time=Math.abs(a.diff(b, 'minutes')); // true\
                                         console.log(time);
                                       res.send("hello");
                                   }
                               });


                           }
                       });
                   }
              });
         }
});
});

module.exports = router;