/**
 * The node-module to hold the constants for the server
 */


function define(obj, name, value) {
    Object.defineProperty(obj, name, {
        value:        value,
        enumerable:   true,
        writable:     false,
        configurable: false
    });
}


exports.responseStatus = {};

define(exports.responseStatus, "PARAMETER_MISSING", 100);
define(exports.responseStatus, "INVALID_ACCESS_TOKEN", 101);
define(exports.responseStatus, "ERROR_IN_EXECUTION", 102);
define(exports.responseStatus, "SHOW_ERROR_MESSAGE", 103);
define(exports.responseStatus, "SHOW_MESSAGE", 104);
define(exports.responseStatus, "SHOW_DATA", 105);

exports.responseMessage = {}; define(exports.responseMessage, "PARAMETER_MISSING", "Some Parameters Missing");
define(exports.responseMessage, "INVALID_ACCESS_TOKEN", "Please try again, Invalid access.");
define(exports.responseMessage, "ERROR_IN_EXECUTION", "Some error occurred. Please try again.");
define(exports.responseMessage, "SHOW_ERROR_MESSAGE", "Some error occurred. Please try again.");
define(exports.responseMessage, "SHOW_MESSAGE", "Hi there!");
define(exports.responseMessage, "SHOW_DATA", "Successfully  execute the data");
define(exports.responseMessage, "USER_INSERT", "successfully insert the user");
define(exports.responseMessage, "DRIVER_INSERT", "Successfully insert the driver");
define(exports.responseMessage, "NO_DRIVER_FREE", "No driver free that time");
define(exports.responseMessage, "EMAIL_EXISTS", "this email is already registered");
define(exports.responseMessage, "INVALID_EMAIL", "You ENTER WRONG EMAIL");
define(exports.responseMessage, "NOT_REGISTERED", "this email is not registered plase register first");
define(exports.responseMessage, "INVAILD_PASSWORD", "enter invaild password. please try again");
define(exports.responseMessage, "CHANGE_PASSWORD", "Successfully change  the password");
define(exports.responseMessage, "LOGOUT_USER", "Succesfully logout the user");
define(exports.responseMessage, "SEND_DATA", "successfully send the mail");
define(exports.responseMessage, "INVALID_EMAIL", "You ENTER WRONG EMAIL");
define(exports.responseMessage, "EXPIRE_SESSION", "Expires the session and wrong the sesssion");
define(exports.responseMessage, "INVALID_DRIVER", "Driver does not accepted through the admin please again verify the driver");
define(exports.responseMessage, "UPDATE_PROFILE", "Sccessfully update the profile");
define(exports.responseMessage, "VERIFIED_DRIVER", "Successfully verified the driver");
define(exports.responseMessage, "NOT_ACCEPTED_DRIVER", "These driver not accepted through the admin");
define(exports.responseMessage, "ALREADY_ACCEPTED", "Driver already accepted ");
define(exports.responseMessage, "REJECTED_DRIVER", "We're sorry, your application has been declined  the driver");
define(exports.responseMessage, "ASSIGN_DRIVER", "status message is wrong plz select 0 for rejected and 1 for accepted");
define(exports.responseMessage, "IN_RADIUS_NO_DRIVER", "In this area no driver ");
define(exports.responseMessage, "NOT_ACCEPTED_ANY_DRIVER", "does not accepted any driver");
define(exports.responseMessage, "ACCEPTED_DRIVER", "driver accepted the order from the customer");
define(exports.responseMessage, "DRIVER_NOT_REACH", "driver does not reach that time pick up for the customer");
define(exports.responseMessage, "NO_ORDER", "that driver does not provide any order plz check again");

exports.deviceType = {};
define(exports.deviceType, "ANDROID",   1);
define(exports.deviceType, "iOS",       2);