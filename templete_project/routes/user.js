var express = require('express');
var router = express.Router();
var func=require('./commonFunction');
var sendResponse=require('./sendResponse');
var async=require('async');
var md5 = require('MD5');

/* GET users listing. */


//Signup user
router.post('/signup_user',function(req,res,next){
   // console.log(req.body);
    var firstName = req.body.first_name;
    var email = req.body.email;
    var password = req.body.password;
    var phonoNo=req.body.phonono;
    var address=req.body.address;
    var curLatitude=req.body.current_latitude;
    var curLongitude=req.body.current_longitude;
    var deviceType = req.body.device_type; // 1 for android, 2 for ios
    var deviceToken = req.body.device_token;
    var appVersion = req.body.app_version;  //1 for android.3 for ios
    console.log(req.body.file1);
    var userImage=req.files.file;
    var manValues=[firstName,email,password,phonoNo,address,curLatitude,curLongitude,deviceType,deviceToken,appVersion,userImage];
    var randomFileName=func.generateRandomNumber();
    req.files.file.name = randomFileName;
   // var userImage=req.files.file1;
    async.waterfall([
        function(callback) {
            func.checkBlank(res, manValues, callback);
        },
        function(callback) {
           func.checkEmailAvailability(res, email, callback);
        }
    ],function(err,result){
        console.log("async result",result);

        if(err){
            sendResponse.somethingWentWrongError(res);
        }
        else {
            var curLoginTime = new Date();
            var accessToken = func.encrypt(email + curLoginTime);
            var encryptPassword = md5(password);
            console.log("insert statement");

            func.uploadImage(res,userImage,function(path) {

                console.log(path);
                var sql = "INSERT INTO `tb_user`(`name`, `email`, `password`,`access_token`,`phonono`,`address`,`current_latitude`,`current_longitude`,`device_type`,`device_token`,`app_version`,`signup_time`,`current_status`,`image`,reg_as) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                var values = [firstName, email, encryptPassword, accessToken, phonoNo, address, curLatitude, curLongitude, deviceType, deviceToken, appVersion, curLoginTime, 0, path,0];   //0 for registered through user

                dbConnection.Query(res, sql, values, function (result) {
                    console.log("execute the insert query");
                    var data = {
                        access_token: accessToken,
                        first_name: firstName,
                        address_user:address
                    };
                    func.sendMail(email, firstName, res, function (callback) {
                        if (callback == 1)
                            sendResponse.sendSuccessData(data, res);
                    });
                });
            });
        }

    });

});

//login user
router.post('/login_user',function(req,res) {
    var email = req.body.email;
    var pass = req.body.password;
    var latitude=req.body.latitude;
    var longitude=req.body.longitude;
    var deviceType = req.body.device_type; // 1 for android, 2 for ios
    var deviceToken = req.body.device_token;
    var appVersion = req.body.app_version;

    var manValues = [email, pass,latitude,longitude];
   func.checkBlank(res,manValues,function(callback){
           if(callback==null) {
               func.checkEmailRegistered(res,email,function(callbackEmailRegisterd) {
                   console.log("result the callback function", callbackEmailRegisterd);
                   var password1 = callbackEmailRegisterd[0].password;
                   //console.log(password1+"  "+pass +md5(pass));
                   var encrptPassword = md5(pass);
                   console.log(password1 == encrptPassword);
                   if (password1 == encrptPassword) {
                       var id = callbackEmailRegisterd[0].id;
                       var curLoginTime = new Date();
                       var accessToken = func.encrypt(email + curLoginTime);
                       var regAs=callbackEmailRegisterd[0].reg_as;
                       var verifiedStatus=callbackEmailRegisterd[0].verified_status;
                       var currentStatus="";
                       sql1 = 'update `tb_user` set current_status=?,access_token=?,current_latitude=?,current_longitude=?,device_type=?,device_token=?,app_version=? where id=?';

                       if(regAs==0) {
                           values = [verifiedStatus, accessToken, id, latitude, longitude, deviceType, deviceToken, appVersion]; //1 for customer login and 2 for driver login
                           dbConnection.Query(res, sql1, values, function (result) {
                               console.log(result + "define the result");
                               if (result.length != 0) {
                                   console.log("succesfully update the user login table with new access token");
                                   data = {
                                       new_access_token: accessToken,
                                       email: email
                                   };
                                   sendResponse.sendSuccessData(data, res);
                               }
                               else {
                                   sendResponse.somethingWentWrongError(res);
                               }
                           });
                          // currentStatus=1;
                       }
                       else if(regAs==1 && verifiedStatus==1){
                           values = [verifiedStatus, accessToken, id, latitude, longitude, deviceType, deviceToken, appVersion]; //1 for customer login and 2 for driver login
                           dbConnection.Query(res, sql1, values, function (result) {
                               console.log(result + "define the result");
                               if (result.length != 0) {
                                   console.log("succesfully update the user login table with new access token");
                                   data = {
                                       new_access_token: accessToken,
                                       email: email
                                   };
                                   sendResponse.sendSuccessData(data, res);
                               }
                               else {
                                   sendResponse.somethingWentWrongError(res);
                               }
                           });
                             //currentStatus=2;
                       }
                       else if(regAs==1 && verifiedStatus==0){
                            sendResponse.successStatusMsg(constant.responseMessage.INVALID_DRIVER,res);
                       }
                       else{
                           sendResponse.somethingWentWrongError(res);
                       }


                   }
                   else {
                       sendResponse.sendErrorMessage(constant.responseMessage.INVAILD_PASSWORD, res);
                   }
               });
           }

       else{
               sendResponse.somethingWentWrongError(res);
           }
    });
});
//logout function
router.post('/logout',function(req,res) {
    var accessToken = req.body.access_token;
    console.log(accessToken);
    var checkBlank = [accessToken];
    func.checkBlank(res, checkBlank, function (callback) {
        if (callback == null) {
            func.authenticateUser(res,accessToken, function (result) {
                var user_id = result[0].user_id;
                var sql = "UPDATE `tb_user` SET `current_status`=?,`device_token`=? WHERE `id`=? LIMIT 1";   //user status 1 for login and 0 for logout
                dbConnection.Query(res,sql, [0, "", user_id], function (result_update) {
                    console.log("logout update the status");
                    if(result_update.length!=0) {
                        sendResponse.successStatusMsg(constant.responseMessage.LOGOUT_USER, res);
                    }
                    else{
                        sendResponse.somethingWentWrongError(res);
                    }
                });
            });
        }
    });
});


//forget the password send the mail and for changing password
router.post('/forgetpassword',function(req,res){

    //console.log(hostName);
    var email=req.body.email;
    var manValues=[email];

    async.waterfall([
        function(callback) {
            func.checkBlank(res, manValues, callback);
        },
        function(callback) {
            checkEmailAvailability(res, email, callback);
        }
    ],function(result) {
        userId=result.id;
        func.generateOneTimePassword(res,userId,function(callbackPassword) {

            var to = email;
            var msg = "change the Password";
            sub = "<h2> Change the password </h2>";
            sub += "<h5> Hello form the testing app </h5>";
            sub += "http://localhost:"+config.get('PORT')+"/changePassword?link="+callbackPassword;
            func.sendMail2(res, to, sub, msg, function (resultMail) {
                if (resultMail == 1) {
                    console.log("succesffully send the mail");
                    sendResponse.successStatusMsg(constant.responseMessage.SEND_DATA, res);
                }
                else {
                    console.log("this email is wrong");
                    sendResponse.sendErrorMessage(constant.responseMessage.INVAILD_EMAIL, res);
                }
            });

        });
    });
});

//check the email is alredy exist or not
function checkEmailAvailability(res, email, callback){

    var sql='select `id`,reg_as from `tb_user` where `email`=? limit 1';
    var values=[email];
    //console.log("email check");
    dbConnection.Query(res,sql,values,function(userResponse){
        //  accessToken=userResponse[0].access_token;
        console.log(userResponse);
        // console.log(userResponse.length+"jddsd"+accessToken+"  "+id);
        if(userResponse.length>0){
          var id=userResponse[0].id;
            data={
                "id":id,
                "email":email
            };
            return  callback(data);
        }
        else{
            console.log("wrong email");
            sendResponse.sendErrorMessage(constant.responseMessage.INVALID_EMAIL,res);
        }
    });
}
//change password in api
router.post('/changepassword',function(req,res){
   // console.log("hello"+req.params('link'));
    //console.log("link"+req.body('link'));
    var accessToken=req.body.access_token;
    var oldPassword=req.body.old_password;
    var newPassword=req.body.new_password;

    var manValues=[accessToken,oldPassword,newPassword];

    async.waterfall([
        function(callback) {
            func.checkBlank(res, manValues, callback);
        },
        function(callback) {
            func.authenticateUser(res,accessToken,callback)
        }
    ],function(result) {
        console.log("async result", result);
        var id=result[0].id;
        func.checkPasswordAvailable(req,res,id,oldPassword,newPassword);
    });
        /*var manValues=[oldPassword,newPassword,oneTimePassword];
        func.checkBlank(res,manValues,function(callback){
            if(callback==null){
                console.log("hello");
                func.checkPasswordAvailable(req,res,oldPassword,newPassword,oneTimePassword);
            }
            else{
                sendResponse.somethingWentWrongError(res);
            }
        });*/

});

router.post('/changepasswordonetimelink',function(req,res){
    //  console.log("one time password link value",x);

         var oneTimePassword=req.body.one_time_password;
         var password=req.body.new_password;

         var manValues=[oneTimePassword,password];

    async.waterfall([
        function(callback) {
            func.checkBlank(res, manValues, callback);
        },
        function(callback) {
            func.checkOneTimeLink(res,oneTimePassword,callback);
        }
    ],function(result) {
        var userId=result[0].id;
        var newPassword = md5(password);
        sql1='update tb_user set password=?,one_time_password=? where id=?';
        dbConnection.Query(res,sql1,[newPassword," ",userId],function(result){
            var effectrows = result.affectedRows;
            if(effectrows==0){
                console.log(effectrows);
                sendResponse.somethingWentWrongError(res);
            }
            else{
                sendResponse.successStatusMsg(constant.responseMessage.CHANGE_PASSWORD,res);
            }
        });
    });

});

// edit profile api for user and driver
router.post('/edit_profile',function(req,res){
    var accessToken=req.body.access_token;
    var firstName = req.body.first_name;
    var phonoNo=req.body.phonono;
    var address=req.body.address;
    var image=req.files.file;
    var manValues=[accessToken,firstName,phonoNo,address,image];
    async.waterfall([
        function(callback) {
            func.checkBlank(res, manValues, callback);
        },
        function(callback) {
            func.authenticateUser(res, accessToken, callback);
        }
    ],function(result) {
        var userId=result[0].id;
        var image_pic=image.size;
     if(image_pic==0){
         sql1='update tb_user set name=?,phonono=?,address=? where id=? limit 1';
         var values=[firstName,phonoNo,address,userId];
         dbConnection.Query(res,sql1,values,function(result){
             if(result!=0){
                 sendResponse.successStatusMsg(constant.responseMessage.UPDATE_PROFILE,res);
             }
             else{
                 sendResponse.somethingWentWrongError(res);
             }
         });
     }
        else{
         var randomFileName=func.generateRandomNumber();
         req.files.file.name = randomFileName;
         func.uploadImage(res,image,function(path) {
             sql1 = 'update tb_user set name=?,phonono=?,address=?,image=? where id=? limit 1';
             var values = [firstName, phonoNo, address,path,userId];
             dbConnection.Query(res, sql1, values, function (result) {
                 if (result != 0) {
                     sendResponse.successStatusMsg(constant.responseMessage.UPDATE_PROFILE, res);
                 }
                 else {
                     sendResponse.somethingWentWrongError(res);
                 }
             });
         });
     }

    });

});



module.exports = router;
