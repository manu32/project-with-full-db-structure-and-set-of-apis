/**
 * Created by ABC on 3/17/2015.
 */
var express = require('express');
var router = express.Router();
var async=require('async');
var func=require('./commonFunction');
var sendResponse=require('./sendResponse');

//this api is used to get the driver that is available in given radius
router.post('/get_driver',function(req,res){
    var accessToken=req.body.access_token;
    var centralLatitude=req.body.latitude;
    var centralLongitude=req.body.longitude;
    var radius=req.body.radius; //comes in the miles
    var radius=radius * 1609.34;  //convert miles into the meters

    var manValues=[accessToken,centralLatitude,centralLongitude,radius];

    async.waterfall([
        function(callback){
            func.checkBlank(res,manValues,callback);
        },
        function(callback){
            func.authenticateAdmin(res,accessToken,callback);
        }
    ], function(callbackresult){
        if(callbackresult==1) {
            var withInRadiusDriver=[];
            sql1='select `id`,`name`,`current_latitude`,`current_longitude` from tb_user where reg_as=? && verified_status=? && idle_driver=?';
            var values=[1,1,1];  //first one indicate the driver and other one tells the accepted driver through the admin
            dbConnection.Query(res,sql1,values,function(resultVerifiedDriver){
                if(resultVerifiedDriver.length>0){

                    for(var i=0;i<resultVerifiedDriver.length;i++){
                        var currentLatitude=resultVerifiedDriver[i].current_latitude;
                        var currentLongitude=resultVerifiedDriver[i].current_longitude;
                        var driverId=resultVerifiedDriver[i].id;
                        var driverName=resultVerifiedDriver[i].name;

                        func.getAutheriseDriver(currentLatitude,currentLongitude,centralLatitude,centralLongitude,radius,function(callbackInsideRadius){
                            if(callbackInsideRadius){
                                func.checkdistance(currentLatitude, currentLongitude, centralLatitude, centralLongitude, function (callbackdist) {
                                    withInRadiusDriver[i] = {
                                        'driver_id': driverId,
                                        'driver_name': driverName,
                                        'latitude': currentLatitude,
                                        'longitude': currentLongitude,
                                        'driver_distance': callbackdist
                                    };
                                });
                                sql1='update tb_user set particular_area_driver_status=? where id=?';
                                var values=[1,driverId];                                              //update the area status for driver available in particular area
                                dbConnection.Query(res,sql1,values,function(result){
                                    var effectRows=result.affectedRows;
                                    if(effectRows==0){
                                        sendResponse.somethingWentWrongError(res);
                                    }
                                });
                            }
                        });
                    }
                    console.log(withInRadiusDriver);
                    var sort = func.sortValue(withInRadiusDriver, 'driver_distance');
                    sendResponse.sendSuccessData(sort, res);
                }
                else if(withInRadiusDriver==0){
                    sendResponse.sendErrorMessage(constant.responseMessage.IN_RADIUS_NO_DRIVER,res);
                }
                else{
                    sendResponse.somethingWentWrongError(res);
                }
            });
        }
        else{
            sendResponse.somethingWentWrongError(res);
        }
    });

});


module.exports = router;